## Notes

# 
to stdout in pipenv
- use echo with '$variable'

pipenv run echo '$PYTHONPATH'
pipenv run echo '$DJANGO_SETTINGS_MODULE'

- PYTHONPATH manipulation is bad practice but this course included it
- with PYTHONPATH as . pipenv run pytest api/coronavstech works but running the following does not:
  - pipenv shell
  - cd api
  - cd coronavstech 
  - pytest
- but in shell running pytest api/coronavstech does work


# Pytest notes: more notes in Stantec Prep notebook
- pytest is easier to read than unittest
- @pytest.mark is a decorator that when placed before a class or function can give it either built in functionality or custom built
  - examples are 
    - .skip which skips the test
    - .xfail which means the function should fail
    - .django_db which creates a test db that is deleted on completion
- Client is imported from django.test and is used as a fake browser request to create, update, get, and delete data
- will need to import json and pytest, (if using unittest will need to import TestCase from unittest library)
- much of what I have learned so far is asserting that the correct response is sent whether that be a failure notification or a match between a response and an instance created