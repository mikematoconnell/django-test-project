
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from api.coronavstech.companies.urls import companies_router
from api.coronavstech.companies.views import send_company_email


router = routers.DefaultRouter()

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include(companies_router.urls)),
    path("send-email/", send_company_email),
    path("fibonacci/", include("api.coronavstech.fibonacci.urls"))
]
