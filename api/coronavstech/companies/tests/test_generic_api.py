import json
import requests
import pytest


test_env_companies_url = "http://127.0.0.1:8000/companies"

@pytest.mark.skip
def test_zero_companies_django_agnostic():
    response = requests.get(url=test_env_companies_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []

@pytest.mark.skip
def test_create_company_with_layoffs_django_agnostic():
    response = requests.post(
        url=test_env_companies_url,
        json={"name": "test company name", "status": "Layoffs"}
    )
    assert response.status_code == 201
    response_content = json.loads(response.content)
    assert response_content.get("status") == "Layoffs"

    cleanup_company(company_id=response_content["id"])

def cleanup_company(company_id: str) -> None:
    response = requests.delete(url=f"http://127.0.0.1:8000/companies/{company_id}")
    assert response.status_code == 204

# do not have auth for this must have change will still write it out
@pytest.mark.skip
def test_doge_coin() -> None:
    response = requests.get(
        url="https://api.cryptonator.com/api/ticker/doge-usd", headers={"User-Agent": "Mozilla/5.0"}
    )

    assert response.status_code == 200

    response_content = json.loads(response.content)
    
    # the following will assert that the structure (scheme) has stayed the same
    assert response_content["ticker"]["base"] == "DOGE"
    assert response_content["ticker"]["target"] == "USD"
    