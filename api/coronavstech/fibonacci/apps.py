from django.apps import AppConfig


class FibonacciConfig(AppConfig):
    name = "api.coronavstech.fibonacci"
