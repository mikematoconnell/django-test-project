import json
import pytest
from django.test import Client
from django.urls import reverse


def test_n_works(client) -> None:
    fib_url = reverse("calculate-fibonacci", kwargs={"n":20})
    response = client.get(path=f"{fib_url}")
    response_value = json.loads(response.content).get("Result")
    assert response.status_code == 200
    assert response_value == 6765

@pytest.mark.xfail
def test_five_fails(client):
    fib_url = reverse("calculate-fibonacci", kwargs={"n":5})
    response = client.get(path=f"{fib_url}")
    response_value = json.loads(response.content).get("Result")
    assert response_value == 5

@pytest.mark.parametrize("n", [25, 65, 84, 45])
def test_stress(client, n):
    fib_url = reverse("calculate-fibonacci", kwargs={"n":n})
    response = client.get(path=f"{fib_url}")
    assert response.status_code == 200

@pytest.mark.parametrize("n, expected", [(1, 1), (20, 6765), (249, 4880197746793002076754294951020699004973287771475874)])
def test_stress_multiple_with_assert(client, n, expected):
    fib_url = reverse("calculate-fibonacci", kwargs={"n": n})
    response = client.get(path=f"{fib_url}")
    response_value = json.loads(response.content).get("Result")
    assert response_value == expected


