from django.shortcuts import render
from django.http import JsonResponse

from api.coronavstech.fibonacci.dynamic import dynamic_fibonacci_v2

# Create your views here.

def calculate_fibonacci(request, n):
    res = dynamic_fibonacci_v2(n)

    return JsonResponse({"Result": res})