from django.urls import path

from .views import (calculate_fibonacci)


urlpatterns = [
    path("calculate_fibonacci/<int:n>/", calculate_fibonacci, name="calculate-fibonacci")
]