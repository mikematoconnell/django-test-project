import pytest

from typing import List, Tuple, Callable, Dict

from fibonacci.naive import fibonacci_naive

from fibonacci.cached import fibonacci_cached, fibonacci_lru_cached
from fibonacci.dynamic import dynamic_fibonacci, dynamic_fibonacci_v2
from fibonacci.fixtures import time_tracker

Decorator = Callable

def get_list_of_kwargs_for_function(
        identifiers: str, values: List[Tuple[str, str]]
) -> List[Dict[str, str]]:
    print(f"getting list of kwargs for function, \n{identifiers=}, {values}")
    parsed_identifiers = identifiers.split(", ")
    list_of_kwargs_for_function = []
    for tuple_value in values:
        kwargs_for_function = {}
        for i, keyword in enumerate(parsed_identifiers):
            kwargs_for_function[keyword] = tuple_value[i]
        list_of_kwargs_for_function.append(kwargs_for_function)
    print(f"{list_of_kwargs_for_function}")
    return list_of_kwargs_for_function


def my_parametrizer(identifiers: str, values: List[Tuple[int, int]]) -> Decorator:
    def my_parametrized_decorator(function: Callable) -> Callable:
        def run_func_parametrized() -> None:
            list_of_kwargs_for_function = get_list_of_kwargs_for_function(
                identifiers=identifiers, values=values
            )
            for kwargs_for_function in list_of_kwargs_for_function:
                print(f"calling function {function.__name__} with {kwargs_for_function}")
                function(**kwargs_for_function)
        return run_func_parametrized
    return my_parametrized_decorator


@pytest.mark.parametrize("fib_func", [fibonacci_naive, fibonacci_cached, fibonacci_lru_cached, dynamic_fibonacci, dynamic_fibonacci_v2])
@pytest.mark.parametrize("n,expected", [(0, 0), (1, 1), (2, 1), (20, 6765)])
def test_fibonacci(time_tracker, fib_func: Callable[[int], int], n: int, expected:int) -> None:
    res = fib_func(n)
    assert res == expected

