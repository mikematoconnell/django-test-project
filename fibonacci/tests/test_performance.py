import pytest

from fibonacci.dynamic import dynamic_fibonacci_v2
from fibonacci.fixtures import track_performance

@pytest.mark.performance
@track_performance
def test_performance():
    dynamic_fibonacci_v2(10000)