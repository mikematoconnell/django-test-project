def dynamic_fibonacci(n):
    fib_list = [0, 1]
    if n == 0 or n == 1:
        return n
    
    for i in range(n - 1):
        fib_list.append(fib_list[-1] + fib_list[-2])
    
    return fib_list[-1]

def dynamic_fibonacci_v2(n):
    fib_list = [0, 1]
    if n == 0 or n == 1:
        return n
    
    for i in range(n - 1):
        new_number = fib_list[0] + fib_list[1]
        fib_list[0] = fib_list[1]
        fib_list[1] = new_number
    
    return fib_list[-1]


def call():
    res = dynamic_fibonacci(20)

    print(res)


if __name__ == "__main__":
    call()